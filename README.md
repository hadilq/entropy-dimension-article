
# The dimension of the spacetime is a measure of Entropy

## Abstract

The entropy as the thermodynamics and statistical mechanics described is the disorder of the system 
and as the information theory described is the unpredictability of the system. These concepts are 
related to how much information content we have in the system. On the other hand, in fractal world 
people found fractal dimension. Here, we find out the fractal dimension and entropy of the fractal 
are related, so the fractal dimension can be used to determine the entropy of the system, which 
means the dimension of the system is a measure of the amount of information content in the system. 
In the end, we'll deduce a consistent proposition that the spacetime is a fractal, so its dimension 
is related to its entropy.


## Conclusion

Here we have two propositions. One for mathematicians, that says the fractal dimension of multi-type 
pattern fractal can be calculated by mean dimension proposition. And one proposition for physicists 
that says spacetime is a fractal space. If these two assumptions be correct then we can have deep 
insights to our universe, such as spacetime exists because of particles inside it or why photons 
are quantized.


## This is a green open access article

As science, just like economics, needs freedom to exist, I encourage all researchers to publish their research 
in git repositories, because this technology is so suitable to keep track of the time events, which is exactly 
what science is needed to flourish.
