\begin{thebibliography}{10}

\bibitem{cropper2001great}
William~H Cropper.
\newblock {\em Great physicists: The life and times of leading physicists from
  Galileo to Hawking}.
\newblock Oxford University Press, 2001.

\bibitem{planck1896gegen}
Max Planck.
\newblock Gegen die neuere energetik.
\newblock {\em Annalen der Physik}, 293(1):72--78, 1896.

\bibitem{jaynes1965gibbs}
Edward~T Jaynes.
\newblock Gibbs vs boltzmann entropies.
\newblock {\em American Journal of Physics}, 33(5):391--398, 1965.

\bibitem{shannon1948mathematical}
Claude~Elwood Shannon.
\newblock A mathematical theory of communication.
\newblock {\em Bell system technical journal}, 27(3):379--423, 1948.

\bibitem{mandelbrot1983fractal}
Benoit~B Mandelbrot.
\newblock {\em The fractal geometry of nature}, volume 173.
\newblock WH freeman New York, 1983.

\bibitem{falcomer1990fractal}
KJ~Falcomer.
\newblock Fractal geometry: Mathematical foundation and applications, 1990.

\bibitem{marstrand1954dimension}
JM~Marstrand.
\newblock The dimension of cartesian product sets.
\newblock In {\em Mathematical Proceedings of the Cambridge Philosophical
  Society}, volume~50, pages 198--202. Cambridge University Press, 1954.

\bibitem{hurewicz1995collected}
Witold Hurewicz and Krystyna Kuperberg.
\newblock {\em Collected Works of Witold Hurewicz}, volume~4.
\newblock American Mathematical Soc., 1995.

\bibitem{renyi1959dimension}
Alfr{\'e}d R{\'e}nyi.
\newblock On the dimension and entropy of probability distributions.
\newblock {\em Acta Mathematica Academiae Scientiarum Hungarica},
  10(1-2):193--215, 1959.

\bibitem{schwarzenberger1990fractal}
Rolph Schwarzenberger.
\newblock Fractal geometry: mathematical foundations and applications, by
  falconer kenneth. pp 288.{\pounds} 19{\textperiodcentered} 95. 1990. isbn
  0-471-92287-0 (wiley).
\newblock {\em The Mathematical Gazette}, 74(469):316--317, 1990.

\bibitem{lifshitz1980landau}
Evgeny~Mikhailovich Lifshitz, LP~Pitaevskii, and VB~Berestetskii.
\newblock Landau and lifshitz course of theoretical physics.
\newblock {\em Statistical physics}, 5, 1980.

\bibitem{landau1968statistical}
Lev~Davidovich Landau and Evgenii~M Lifshitz.
\newblock {\em Statistical physics: V. 5: course of theoretical physics}.
\newblock Pergamon press, 1968.

\bibitem{planck1914theory}
MKEL Planck.
\newblock The theory of heat radiation, translated by m.
\newblock {\em Masius, P. Blackiston’s Son \& Co, Philadelphia, reprinted by
  Kessinger}, 1914.

\bibitem{einstein1905erzeugung}
Albert Einstein.
\newblock {\"U}ber einen die erzeugung und verwandlung des lichtes betreffenden
  heuristischen gesichtspunkt.
\newblock {\em Annalen der physik}, 322(6):132--148, 1905.

\bibitem{french1979introduction}
Anthony~Philip French and Edwin~F Taylor.
\newblock {\em An introduction to quantum physics}.
\newblock CRC Press, 1979.

\end{thebibliography}
